#!/bin/sh
#
### BEGIN INIT INFO
# Provides:       moosefs-metalogger
# Required-Start: $local_fs $network $syslog $remote_fs $named $time
# Required-Stop:  $local_fs $syslog $remote_fs
# Should-Start:   $syslog
# Should-Stop:    $network $syslog
# Default-Start:  2 3 4 5
# Default-Stop:   0 1 6
# Short-Description: MooseFS metalogger daemon
# Description:       MooseFS metalogger daemon
### END INIT INFO

DAEMON=/usr/sbin/mfsmetalogger
NAME=moosefs-metalogger
CFGFILE=/etc/mfs/mfsmetalogger.cfg

# Exit if executable is not installed
[ -x $DAEMON ] || exit 0

# Read configuration variable file if it is present
DEFAULTS_FILE=/etc/default/${NAME}
[ -r "$DEFAULTS_FILE" ] && . $DEFAULTS_FILE

PIDF=/var/run/${NAME}.pid
RETRY=TERM/30/KILL/5

# Load the VERBOSE setting and other rcS variables
[ -f /etc/default/rcS ] && . /etc/default/rcS

# define LSB log_* functions.
. /lib/lsb/init-functions

case "$1" in
    start)
        if $0 status >>/dev/null; then
            log_action_msg "$NAME is already running"
            exit 0
        fi
        log_action_begin_msg "$NAME starting"
        if R=$(start-stop-daemon --exec $DAEMON --pidfile ${PIDF} --make-pidfile \
            --start --background --oknodo -- -c ${CFGFILE} start 2>&1);
        then
            log_action_end_msg 0 "$R"
        else
            log_action_end_msg 1 "$R"
        fi
    ;;
    stop)
        log_action_begin_msg "$NAME stopping"
        if R=$(start-stop-daemon --exec $DAEMON --pidfile ${PIDF} --remove-pidfile \
             --stop --retry=$RETRY --quiet);
        then
            log_action_end_msg 0 "$R"
        else
            log_action_end_msg 1 "not running"
        fi
    ;;
    reload)
        log_action_begin_msg "Reloading $NAME configuration"
        if start-stop-daemon --exec $DAEMON --pidfile ${PIDF} \
            --stop --signal HUP --quiet;
        then
            log_action_end_msg 0
        else
            log_action_end_msg 1
        fi
    ;;
    force-reload|restart)
        $0 stop
        $0 start
    ;;
    status)
        ## return status 0 if process is running.
        status_of_proc -p ${PIDF} "$DAEMON" "$NAME"
    ;;
    *)
        N=/etc/init.d/$NAME
        echo "Usage: $N {start|stop|restart|reload|force-reload|status}" >&2
    ;;
esac
